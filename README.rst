ShadowOS (Alpine flavor)
========================

ShadowOS is a project aiming to run Shadow on any computer.

ShadowOS is a small footprint live OS based on `Alpine Linux`_ and able to run
Shadow_ as fast as possible on most of x86_64 computers.

This is the sister project of `the original ShadowOS based on Arch Linux`__.

.. __: https://gitlab.com/NicolasGuilloux/shadow-live-os/

.. _Alpine Linux: https://alpinelinux.org/
.. _Shadow: https://shadow.tech/

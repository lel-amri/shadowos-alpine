profile_shadowos() {
	title="ShadowOS"
	desc="ShadowOS is a project aiming to run Shadow on any computer."
	profile_base
	image_ext="iso"
	arch="x86_64"
	output_format="iso"
	kernel_flavors="shadowos"
	kernel_addons="virtualbox-guest-modules"
	apks="$apks
		openbox xorg-server virtualbox-guest-additions
		virtualbox-guest-modules-shadowos eudev xf86-input-libinput
		xf86-video-amdgpu xf86-video-intel xf86-video-nouveau xf86-video-vmware
		xf86-video-qxl xf86-video-modesetting pciutils shadowapp lxterminal
		networkmanager stalonetray network-manager-applet polkit polkit-gnome
		consolekit2 dbus-x11 setxkbmap libc6-compat gconf nss libxscrnsaver
		libxtst alsa-lib
		"
	hostname="shadowos"
	apkovl="genapkovl-shadowos.sh"
	syslinux_timeout="2"  # Same timeout than for GRUB (Unconfigurable)
}

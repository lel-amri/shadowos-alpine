Files ``mkimg.shadowos.sh`` and ``genapkovl-shadowos.sh`` are to be put with
files in ``scripts/`` from `the official Alpine aports repo
<alpine-aports_>`__.

The directory ``aports`` is an "overlay" for `the original Alpine aports
repo <alpine-aports_>`__.

All this work is based on Alpine version 3.8.2 (``71ed45664a``).

.. _alpine-aports: https://git.alpinelinux.org/aports/

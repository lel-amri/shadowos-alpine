#!/bin/sh -e

HOSTNAME="$1"
if [ -z "$HOSTNAME" ]; then
	echo "usage: $0 hostname"
	exit 1
fi

cleanup() {
	rm -rf "$tmp"
}

makefile() {
	OWNER="$1"
	PERMS="$2"
	FILENAME="$3"
	cat > "$FILENAME"
	chown "$OWNER" "$FILENAME"
	chmod "$PERMS" "$FILENAME"
}

rc_add() {
	mkdir -p "$tmp"/etc/runlevels/"$2"
	ln -sf /etc/init.d/"$1" "$tmp"/etc/runlevels/"$2"/"$1"
}

tmp="$(mktemp -d)"
trap cleanup EXIT

mkdir -p "$tmp"/etc
makefile root:root 0644 "$tmp"/etc/hostname <<EOF
$HOSTNAME
EOF

mkdir -p "$tmp"/etc/apk
makefile root:root 0644 "$tmp"/etc/apk/world <<EOF
alpine-base
openbox
xorg-server
eudev
pciutils
virtualbox-guest-additions
virtualbox-guest-modules-shadowos
shadowapp
lxterminal
networkmanager
stalonetray
network-manager-applet
polkit
polkit-gnome
consolekit2
dbus-x11
setxkbmap
libc6-compat
gconf
nss
libxscrnsaver
libxtst
alsa-lib
EOF

mkdir -p "$tmp"/etc/init.d
makefile root:root 0755 "$tmp"/etc/init.d/firstboot-shadowos <<EOF
#!/sbin/openrc-run

start() {
	rm -f /etc/runlevels/*/\$RC_SVCNAME
	local rc=0
	ebegin "Starting \${RC_SVCNAME}"
	grep shadowos: /etc/group >/dev/null 2>&1 || addgroup -g 1000 shadowos
	grep shadowos: /etc/passwd >/dev/null 2>&1 || (adduser -h /home/shadowos -s /bin/ash -G shadowos -D -H -u 1000 shadowos ; adduser shadowos plugdev ; passwd -d shadowos)
	setup-xorg-base
	eend \$rc
}
EOF

mkdir -p "$tmp"/etc/xdg/openbox
makefile root:root 0644 "$tmp"/etc/xdg/openbox/menu.xml <<EOF
<?xml version="1.0" encoding="UTF-8"?>
<openbox_menu xmlns="http://openbox.org/3.4/menu">
  <menu id="options-keyboard-menu" label="Keyboard Layouts">
    <item label="QWERTY (UK)">
      <action name="Execute">
        <command>setxkbmap -layout gb</command>
      </action>
    </item>
    <item label="QWERTY (US)">
      <action name="Execute">
        <command>setxkbmap -layout us</command>
      </action>
    </item>
    <item label="QWERTZ (DE)">
      <action name="Execute">
        <command>setxkbmap -layout de</command>
      </action>
    </item>
    <item label="AZERTY (FR)">
      <action name="Execute">
        <command>setxkbmap -layout fr</command>
      </action>
    </item>
    <item label="QWERTY (SE)">
      <action name="Execute">
        <command>setxkbmap -layout se</command>
      </action>
    </item>
    <item label="QWERTY MAC (UK)">
      <action name="Execute">
        <command>setxkbmap -layout gb -model macintosh</command>
      </action>
    </item>
    <item label="QWERTY MAC (US)">
      <action name="Execute">
        <command>setxkbmap -layout us -model macintosh</command>
      </action>
    </item>
    <item label="QWERTZ MAC (DE)">
      <action name="Execute">
        <command>setxkbmap -layout de -model macintosh</command>
      </action>
    </item>
    <item label="AZERTY MAC (FR)">
      <action name="Execute">
        <command>setxkbmap -layout fr -model macintosh</command>
      </action>
    </item>
  </menu>
  <menu id="root-menu" label="ShadowOS">
    <separator label="Applications"/>
    <item label="Web browser">
      <action name="Execute">
        <command>epiphany</command>
      </action>
    </item>
    <item label="VirtualHere Server">
      <action name="Execute">
        <command>virtualhere -b</command>
      </action>
    </item>
    <item label="Shadow">
      <action name="Execute">
        <command>/opt/shadowbeta/shadow-beta</command>
      </action>
    </item>
    <separator label="Configurations"/>
    <item label="Bluetooth">
      <action name="Execute">
        <command>blueberry</command>
      </action>
    </item>
    <item label="Sound">
      <action name="Execute">
        <command>pavucontrol</command>
      </action>
    </item>
    <menu id="options-keyboard-menu"/>
    <separator label="System"/>
    <item label="Terminal">
      <action name="Execute">
        <command>lxterminal</command>
      </action>
    </item>
    <separator/>
    <item label="Shutdown">
      <action name="Execute">
        <command>dbus-send --system --dest=org.freedesktop.ConsoleKit --type=method_call /org/freedesktop/ConsoleKit/Manager org.freedesktop.ConsoleKit.Manager.Stop</command>
        <prompt>Shutdown</prompt>
      </action>
    </item>
    <item label="Reboot">
      <action name="Execute">
        <command>dbus-send --system --dest=org.freedesktop.ConsoleKit --type=method_call /org/freedesktop/ConsoleKit/Manager org.freedesktop.ConsoleKit.Manager.Restart</command>
        <prompt>Reboot</prompt>
      </action>
    </item>
  </menu>
</openbox_menu>
EOF

mkdir -p "$tmp"/home/shadowos "$tmp"/home/shadowos/.config/openbox
chown -R 1000:1000 "$tmp"/home/shadowos
makefile 1000:1000 0755 "$tmp"/home/shadowos/.xinitrc <<EOF
exec ck-launch-session dbus-launch --sh-syntax --exit-with-session openbox-session
EOF
makefile 1000:1000 0755 "$tmp"/home/shadowos/.config/openbox/autostart <<EOF
/usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1 &
stalonetray --dockapp-mode simple &
nm-applet --no-agent &
/opt/shadowbeta/shadow-beta &
EOF

# default boot services taken from the official Alpine init from the initramfs
rc_add devfs sysinit
rc_add dmesg sysinit
rc_add mdev sysinit
rc_add hwdrivers sysinit
rc_add modloop sysinit

rc_add hwclock boot
rc_add modules boot
rc_add sysctl boot
rc_add hostname boot
rc_add bootmisc boot
rc_add syslog boot

rc_add mount-ro shutdown
rc_add killprocs shutdown
rc_add savecache shutdown

rc_add firstboot-shadowos default
rc_add networkmanager default

tar -c -C "$tmp" etc home/shadowos | gzip -9n > $HOSTNAME.apkovl.tar.gz

How to build
============

You'll need to build the image on an AlpineLinux system. Everything have to be
run as a normal user, unless specified otherwise. The normal user must be in
the ``abuild`` group.

* Fetch the official aports repo at the right version: ``git clone
  --branch v3.8.2 --depth 1 git://git.alpinelinux.org/aports``

* Copy the files ``mkimg.shadowos.sh`` and ``genapkovl-shadowos.sh`` to the
  ``scripts`` directory of the official aports repo

* Build all the packages in the shadowos aports "overlay"

  * Move into each directory containing an ``APKBUILD`` file, then run
    ``abuild -r``

    This will build the package, and "install" it in a local repo on your
    home directory.

    You will have to run ``abuild-keygen -ai`` beforehand. This will create
    the packages signing keys. (This will create a key pair, add it to the
    user abuild configuration, then install it system-side)

    This have to be done only the first time you build the ShadowOS image, then
    every time a package is updated.

* Move to the ``scripts`` directory in the official aports repo and run
  ``./mkimage.sh --hostkeys --profile shadowos
  --repository $HOME/packages/main
  --repository $HOME/packages/non-free
  --repository $HOME/packages/community
  --repository http://alpine.42.fr/v3.8/main
  --repository http://alpine.42.fr/v3.8/community``. This will build the
  ShadowOS image iso.

  You can replace the mirror ``alpine.42.fr`` by the mirror of your choice.

  The ``--hotkeys`` option will copy the host apk keys to the image. Without
  them the packages you built from the local aports repo could not be
  verified.

  The ``--profile shadowos`` option means you want to build the ShadowOS image.

  The ``--repository REPO`` options are used to specify the repositories to
  use for the image creation they will not be kept in the final image.
